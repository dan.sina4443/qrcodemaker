import pyqrcode
import png
import time
import image
from PIL import Image, ImageDraw

print("Gib bitte den Namen für den QRcode an!")
name = input("name>>")

print("Gib bitte einen Link an der zu einem QRCode umgewandelt werden soll!")
eingabe = input("url>>")

print("Jetzt farbe 1! Mögliche eingaben = grün, blau, rot")
farbe1 = input("farbe1>>")

if farbe1 == "grün":
    farbe1 = 124,252,0
if farbe1 == "blau":
    farbe1 = 30,144,255
if farbe1 == "rot":
    farbe1 = 255,0,0



print("Jetzt farbe 2!")
farbe2 = input("farbe2>>")
if farbe2 == "grün":
    farbe2 = 124,252,0
if farbe2 == "blau":
    farbe2 = 30,144,255
if farbe2 == "rot":
    farbe2 = 255,0,0

url = pyqrcode.create(eingabe, error='H')
url.png("qrcodes/" + name + ".png", module_color=(farbe1), background=(farbe2), scale=(10))
print("Der QRCode wird erstellt......")
print("Der QRCode wurde in dem Ordner >> qrcodes << erstellt \r\n" +
      "Wenn da kein Ordner namens >> qrcodes << vorhanden ist \r\n" +
      "bitte erstelle ihn sonst kommt ein Error!")

myImage = Image.open("qrcodes/" + name + ".png");
myImage.show();

time.sleep(10)
